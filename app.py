from flask import Flask, request, jsonify
from flask_cors import CORS
from apscheduler.scheduler import Scheduler
import readConfigurationsFile as conf
import classificationController as classification
import atexit
import json
import requests
import constants

configurations = {}
conf.loadConfiguration('settings.conf', configurations)
print(configurations)

app = Flask(__name__)
CORS(app)

cron = Scheduler(deamon=True)
cron.start()

def training_task():
    classification.training()


@app.route('/test', methods=['GET'])
def test():
    return 'Test Correct!'

@app.route('/intentTree', methods=['POST'])
def intent_tree():
    intents = json.loads(request.data.decode('utf-8'))
    classification.create_tree(intents)
    return('200')

@app.route('/addActions', methods=['POST'])
def add_action():
    action = json.loads(request.data.decode('utf-8'))
    context_json = json.dumps(action)

    print('Sending action to mongoAPI')
    print(context_json)
    response = requests.post(constants.URL, context_json)

    return str(response)


@app.route('/dynamic', methods=['POST'])
def dynamic():
    context = json.loads(request.data.decode('utf-8'))

    print('Actual context')
    print(context)

    print('Predicting dynamic intents')
    dynamic_intents = classification.get_intents(context)

    return jsonify(dynamic_intents)

@app.route('/log', methods=['POST'])
def add_log():
    log  = json.loads(request.data.decode('utf-8'))
    context_json = json.dumps(log)

    print('Sending log to mongoAPI')
    print(context_json)
    response = requests.post(constants.log_URL, context_json)

    return str(response)

cron.add_cron_job(training_task, args=None, kwargs=None, hour=8, minute=31, day_of_week='Sun')

# Shutdown cron thread if the API is stopped
atexit.register(lambda: cron.shutdown(wait=False))

if __name__ == '__main__':
    app.run(host=str(configurations['HOST']), port=str(configurations['PORT']))

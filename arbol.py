# definicion de arbol
class Arbol:
    def __init__(self, elemento):
        self.hijos = []
        self.elemento = elemento


def buscar_subarbol(arbol, elemento):
    if arbol.elemento == elemento:
        return arbol
    for subarbol in arbol.hijos:
        arbolBuscado = buscar_subarbol(subarbol, elemento)
        if arbolBuscado != None:
            return arbolBuscado
    return None

def buscar_nodos_hojas(arbol, lista):
    if len(arbol.hijos) == 0:
        for key, value in arbol.elemento.items():
            lista.append(str(key))

    for hijo in arbol.hijos:
        buscar_nodos_hojas(hijo, lista)

def agregar_elemento(arbol, elemento, elementoPadre):
    subarbol = buscar_subarbol(arbol, elementoPadre)
    subarbol.hijos.append(Arbol(elemento))


def ejecutar_recorrido_profundidad(arbol):
    print(arbol.elemento)
    for hijo in arbol.hijos:
        ejecutar_recorrido_profundidad(hijo)


def profundidad(arbol):
    if len(arbol.hijos) == 0:
        return 1
    return 1 + max(map(profundidad, arbol.hijos))

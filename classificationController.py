import pandas as pd
import numpy as np
import datetime
import constants, arbol
import requests
import copy
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, f1_score
from sklearn.ensemble import ExtraTreesClassifier
from joblib import dump, load
from os import listdir
from os.path import isfile

# diccionario con los IDs de los usuarios cuyos modelos ya han sido generados
model_users = {}

# diccionario con las columnas utilizadas durante el entrenamiento
columns = {}

# diccionario con los excutedIntents del entrenamiento
model_intents = {}

# arbol de intents del bot
new_arbol = arbol.Arbol({str(-1): 0.0})


# Funcion encargada de crear arbol de intents para saber predicciones nodos intermedios
def create_tree(intents):
    global new_arbol

    # creacion del arbol de intents
    for row in intents:
        if row[3] == 1:
            arbol.agregar_elemento(new_arbol, {str(row[0]): 0.0}, {str(-1): 0.0})

    for row in intents:
        if arbol.buscar_subarbol(new_arbol, {str(row[0]): 0.0}) != None:
            sub_arbol = arbol.buscar_subarbol(new_arbol, {str(row[0]): 0.0})
            if row[4] != None:
                row_hijos = row[4].split(',')
                for item in row_hijos:
                    arbol.agregar_elemento(new_arbol, {str(item): 0.0}, sub_arbol.elemento)

    arbol.ejecutar_recorrido_profundidad(new_arbol)


# Funcion encargada de la obtencion de los intents dinamicos
def get_intents(context):
    global model_users
    global columns
    global model_intents
    dict = {}

    # se obtienen los IDs de los usuarios cuyos modelos ya han sido generados
    list_models = get_models("models/")
    userID = str(context['context']['userID'])

    # comprobar la existencia del modelo del usuario entre los modelos ya generados
    if userID not in list_models:
        print('Usuario no encontrado')
    else:
        if userID not in model_users:
            print("Modelo no cargado")

            # Cargar el modelo
            model = load("models/" + userID)

            # guardar el modelo en la variable global model_users
            model_users[userID] = model

            # guardar las columnas utilizadas durante el entrenamiento
            columns_file = open("columns/" + userID + "_X.txt", 'r')
            columns_list = columns_file.read().splitlines()
            columns_list = [item for item in columns_list if item]
            columns[userID] = columns_list

            # guardar los excutedIntents del entrenamiento
            executed_file = open("model_intents/" + userID + "_y.txt", 'r')
            executed_list = executed_file.read().splitlines()
            executed_list = [item for item in executed_list if item]
            model_intents[userID] = executed_list
        else:
            print("Modelo ya cargado")
            model = model_users[userID]

        dict = processing(model, context, userID)
    return dict


# Funcion encargada del procesamiento y la prediccion de los intents dinamicos
def processing(model, context, userID):
    dynamic_intents = {}

    context_aux = {}
    context_aux['intent'] = context['intent']
    context_aux['context.userID'] = context['context']['userID']
    context_aux['context.day'] = context['context']['day']
    context_aux['context.month'] = context['context']['month']
    context_aux['context.hour'] = context['context']['hour']
    context_aux['context.timestamp'] = context['context']['timestamp']

    # creacion del dataframe del contexto actual
    dataframe = pd.DataFrame(context_aux, index=[0])

    # procesamiento del dataframe
    dataframe = pd.get_dummies(dataframe, columns=['intent', 'context.day'])
    dataframe = dataframe.drop(['context.userID', 'context.timestamp', 'context.month'], axis=1)

    # Matching de las columnas
    # Eliminar columnas que no estan en el entrenamiento
    columns_list = columns[userID]
    for col in dataframe.columns:
        if col not in columns_list:
            dataframe = dataframe.drop([col], axis=1)

    # Anadir columnas no existentes en el dataframe y ordenarlas
    i = 0
    for item in columns_list:
        if item not in dataframe.columns:
            dataframe.insert(i, item, 0)
        i = i + 1

    print(dataframe.head(30))

    # Realizar la prediccion
    prediction = model.predict_proba(dataframe)
    print(prediction)

    # Obtener la maxima precision y su posicion
    max_prediction = np.amax(prediction)
    index = np.where(prediction == max_prediction)

    model_intent_list = model_intents[userID]

    dynamic_intent = model_intent_list[index[1][0]]

    # anadirlo a un diccionario para enviar tanto el intent como su prediccion
    dynamic_intents[dynamic_intent] = max_prediction

    # CALCULAR PREDICCIONES NODOS INTERMIEDIOS
    list_prediction = prediction.tolist()
    diccionario = dict(zip(model_intent_list, list_prediction[0]))

    # obtener subarbol para recomendar intents que penden del nodo situado
    global new_arbol
    arbol_predicciones = copy.deepcopy(new_arbol)
    sub_arbol = arbol.buscar_subarbol(arbol_predicciones, {str(context_aux['intent']): 0.0})

    # asignar predicciones de los nodos finales al arbol
    assign_predictions_tree(sub_arbol, diccionario)

    # calcular las predicciones nodos intermedios a excepcion del nodo actual
    middle_predictions(sub_arbol, str(context_aux['intent']))

    arbol.ejecutar_recorrido_profundidad(sub_arbol)

    # obtener la relacion de los intents y sus predicciones
    predictions = []
    get_intent_predictions(sub_arbol, predictions)

    # obtener la maxima prediccion
    max = 0
    dict_midle={}
    for item in predictions:
        for key, value in item.items():
            if value != 0.0:
                if value > max:
                    max = value
                    dict_midle = item

    for key, value in dict_midle.items():
        dynamic_intents[key] = value
    return dynamic_intents


# Funcion encargada de asignar las predicciones de los nodos finales al arbol
def assign_predictions_tree(arbol, diccionario):
    for key, value in arbol.elemento.items():
        if key in diccionario:
            arbol.elemento = {str(key): diccionario[key]}
    for hijo in arbol.hijos:
        assign_predictions_tree(hijo, diccionario)

# Funcion que calcula las predicciones de nodos intermedios a excepcion del nodo actual
def middle_predictions(arbol, actual):
    sum = 0
    if len(arbol.hijos) > 1:
        for hijo in arbol.hijos:
            for key, value in hijo.elemento.items():
                sum = sum + value
        for key, value in arbol.elemento.items():
            if key != actual:
                if value != -1:
                    arbol.elemento = {str(key): sum}
            else:
                print("es nodo actual")
                arbol.elemento = {str(key): -1}
                for hijo in arbol.hijos:
                    for key, value in hijo.elemento.items():
                        hijo.elemento = {str(key): -1}

    for hijo in arbol.hijos:
        middle_predictions(hijo, actual)

# Funcion encargada de obtener la relacion de los intents y sus predicciones
def get_intent_predictions (arbol, predictions):
    for key, value in arbol.elemento.items():
        # no se consideran los intents del nodo actual
        if value != -1:
            predictions.append(arbol.elemento)
    for hijo in arbol.hijos:
        get_intent_predictions(hijo, predictions)


# Funcion encargada del entrenamiento mediante la generacion de los modelos
def training():
    timestamp = str(datetime.datetime.now())
    print(timestamp + '\n')

    # Limpieza de las variables globales
    global model_users
    global columns
    global model_intents

    model_users.clear()
    columns.clear()
    model_intents.clear()

    # Peticion a la API de Mongo para obtener las acciones
    response = requests.get(constants.URL)
    actions_list = response.json()

    actions_list_aux = []
    actions_dict_aux = {}
    for item in actions_list:
        actions_dict_aux['intent'] = item['intent']
        actions_dict_aux['executedIntent'] = item['executedIntent']
        actions_dict_aux['context.userID'] = item['context']['userID']
        actions_dict_aux['context.day'] = item['context']['day']
        actions_dict_aux['context.month'] = item['context']['month']
        actions_dict_aux['context.hour'] = item['context']['hour']
        actions_dict_aux['context.timestamp'] = item['context']['timestamp']
        actions_list_aux.append(actions_dict_aux.copy())

    dataset = pd.DataFrame.from_dict(actions_list_aux)

    # eliminar del dataset aquellos datos en donde no se ejecuten intents finales
    # lista de intents finales
    # filter_list = ['0', '7', '13', '14', '15', '20', '21', '22', '24', '28', '33', '34', '35', '36', '37', '38', '39',
    # '40', '41', '42', '43', '44', '45', '46', '47', '48',
    # '49', '50', '51', '52', '53']

    filter_list = []
    global new_arbol
    arbol.buscar_nodos_hojas(new_arbol, filter_list)
    dataset = dataset[dataset['executedIntent'].isin(filter_list)]

    # dividir en funcion del userID
    df = pd.DataFrame(dataset)
    # Create a list of unique values by turning the pandas column into a set
    user_list = df['context.userID'].unique()
    for user in user_list:
        # se seleccionan los datos del dataframe en funcion del userID
        print(user)
        df_by_user = df[df['context.userID'] == user]
        df_by_user = pd.get_dummies(df_by_user, columns=['intent', 'context.day'])

        # Separar los datos en caracteristicas y etiquetas
        X = df_by_user.drop(['executedIntent', 'context.userID', 'context.timestamp', 'context.month'], axis=1)
        y = df_by_user['executedIntent']

        # Separar los datos en entrenamiento y testeo
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

        # Guardar las columnas
        x_columns = []
        for col in X.columns:
            x_columns.append(col)

        file = open('columns/' + str(user) + '_X.txt', 'w')

        for item in x_columns:
            file.write('\r%s' % item)
        file.close()

        # Guardar los executedIntents
        y_intents = []
        for col in y.unique():
            y_intents.append(col)

        file = open('model_intents/' + str(user) + '_y.txt', 'w')

        for item in y_intents:
            file.write('\r%s' % item)
        file.close()

        # Instanciar y entrenar el modelo
        # Inicializamos el algoritmo ExtraTreesClassifier e indicamos el numero de arboles que vamos a construir
        classifier = ExtraTreesClassifier(n_estimators=100)

        # Construimos el modelo sobre los datos de entrenamiento
        classifier.fit(X_train, y_train)

        # Predecimos para los valores del grupo Test
        predicciones = classifier.predict(X_test)
        var_accuracy_score = accuracy_score(y_test, predicciones)
        var_f1_score = f1_score(y_test, predicciones, average='macro')

        # Guardar el modelo
        dump(classifier, 'models/' + str(user))

        # Guardar las predicciones
        file = open('results/' + 'predicciones.txt', 'a')
        file.write(timestamp + " user: " + str(user) + " accuracy: " + str(var_accuracy_score) + " f1_score: " + str(
            var_f1_score) + "\n")
        file.close()


# funcion que devuelve los elementos de una ruta dada
def get_models(path):
    return [obj for obj in listdir(path) if isfile(path + obj)]


def loadConfiguration (filename, configurations):
    f = open (filename, "r")
    numlinea = 1
    for line in f:
        line = line.replace(" ","")
        if line.find('#') == 0 or line.__eq__("\n"):
            None
        else:
            if line.find('=') == -1:
                print('[ERROR] ' + str(filename) + ' is not a valid configuration file')
                print('Error in line ' + str(numlinea))
                print('Config value is not valid (' + str(line) + ')')
            else:
                name = line.split('=')[0]
                value = line.split('=')[1]
                value = value.replace("\n", "")
                configurations[name] = value
        numlinea = numlinea+1
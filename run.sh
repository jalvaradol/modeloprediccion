#!/bin/bash
host=localhost
port=8082
export FLASK_APP=./app.py
source venv/bin/activate
flask run -h $host -p $port